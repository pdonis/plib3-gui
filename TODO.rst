plib3.gui To Do List
====================

Planned Features
----------------

- Make sure that coverage is complete for common widget
  methods.

- Enforce consistent handling of class and instance variables
  (e.g., the ``_parent`` variable should be set in the same
  place for all widgets)--this applies particularly to the
  GTK toolkit (lots of cruft in there).

- Enforce common conventions for method and attribute names.

Future Features
---------------

These are features that might be implemented in future
versions of PLIB3.GUI.

- Find a way to have the actual GUI widgets (from the chosen
  toolkit) take their docstrings from the base widgets (which
  define the API).

- Add find/replace functionality to ``PEditControl``.

- Add image and movie display widgets to ``plib.gui``.

- Add an HTML browser widget to ``plib.gui``.

- Is tri-state check box support really needed? Or
  can it be factored out into a separate class
  (``PTriStateCheckBox``)?

- Add center/right justify options to ``PTextLabel``
  (maybe also vertical alignment options).

- The status bar "custom widgets" may be too much;
  may only need to allow more text fields after the
  first, with some way of getting/setting the text.
  Also, add an example program that demonstrates the
  status bar functionality.

- Add a ``WEB`` (or ``WWW``) GUI toolkit that allows the
  same declarative method of specifying a GUI to be used
  in web applications.

Unplanned Features
------------------

These are features that were planned at one point but have
been dropped, unless somebody can convince me that they're
actually worth doing. If I ever do them, it will be because
I have a *lot* of spare time. :-)

- Add Win32 (using the PyWin32 extensions) to the list
  of toolkits supported by ``plib.gui``.

- Add Cocoa (Mac OS X GUI framework) to the list of
  toolkits supported by ``plib.gui``.

Bugs/Workaround Fixes
---------------------

- Setting a font on a group box automatically changes the fonts
  on all its child widgets; need to override implementation to
  only set the font on the groupbox title.

- Embedding a list view in a group box is buggy in Wx;
  the list view spills over the group box border.

- Setting font properties doesn't work in Wx.

- The group box API is inconsistent; a normal groupbox has a
  ``_controls`` private attribute that's a list, while an auto
  group box that initializes itself from specs has a dict for
  that private attribute.

- Need to add a field to panels to keep a list of child widgets.

- In Qt 4 on Windows, it's even weirder: the text color change
  repaints fine, and so does the background color change,
  *except* for the first table row! ???

- Read-only widgets are still in the tab order in Qt/KDE
  (disabled widgets aren't, but the point of read-only widgets
  is to not have user input but look normal, not greyed out).

- Setting background color doesn't appear to work correctly for
  all Qt/KDE 4 widgets.

- Some KDE standard actions don't appear to behave
  consistently with the documentation.

- The wxWidgets "stock" item behavior seems inconsistent;
  the stock item doesn't always override a custom caption
  and image for menu and toolbar items, even though it
  should based on comparison with the corresponding GTK
  code.

- The wx list view column alignment doesn't work (there is an
  API function but it appears to be broken).

- The wx table column alignment doesn't work (there appears
  to be an API function but no documentation about what the
  alignment arguments should be; experimentation has not yet
  figured this out).

- The wxWidgets tab widget is firing a tab changed
  event on shutdown, after the status bar has been
  deleted (causes exception in ``pxmlview.py``--currently
  a hack is being used to mask this).

- The wx window background colors seem all messed up with
  certain color schemes in effect.

- The wx edit control doesn't appear to have APIs for
  some of the desired functionality in the ``pnotepad``
  example program.

- The tab key doesn't move between controls in wx.

- The wx table widget doesn't appear to give good sizing
  information for a main window to wrap to them as a client.

- The current GUI layout code does not support setting
  different margins for top/bottom vs. left/right sides
  of a panel.
